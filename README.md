## loopback-connector-smsapi

Loopback connector module which allow to send emails via sms.

## 1. Installation

````sh
npm install loopback-connector-smsapi --save
````

## 2. Configuration

datasources.json

    {
        "sms": {
            "connector": "loopback-connector-smsapi",
            "token": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
        }
    }

model-config.json

    {
        "Email": {
            "dataSource": "sms",
            "public": false
        }
    }

## 3. Use

Basic option same as built in Loopback. Returns a Promise

    app.models.SMS.send({
        to: "600600600",
        from: "",
        message: "Hello this is sms text message"
    })
    .then(function(response){})
    .catch(function(err){});
