var SMSAPI = require("smsapi");
module.exports = Connector;

/**
 * Configure and create an instance of the connector
 */

function Connector(settings) {
    var token = settings.token || false;

    this.smsapi = new SMSAPI({
      oauth: {
        accessToken: token
      }
    });

}

Connector.initialize = function (dataSource, callback) {

    dataSource.connector = new Connector(dataSource.settings);
    callback();
};

Connector.prototype.DataAccessObject = SMSSender;

/**
 *
 *
 * @constructor
 */
function SMSSender() {

}

/**
 * Send an sms with smsapi
 */
SMSSender.send = function (options, callback) {
    var _this = this;
    var dataSource = _this.dataSource,
    settings = dataSource && dataSource.settings,
    connector = dataSource.connector;


    return connector.smsapi.message
    .sms()
    .from(options.from)
    .to(options.to)
    .message(options.message)
    .execute(); // return Promise


};

/**
 * Send an email instance using instance
 */
SMSSender.prototype.send = function (fn) {
    return this.constructor.send(this, fn);
};

